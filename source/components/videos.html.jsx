export default () => {
  return (
        <div className="section gray padding-top-65 padding-bottom-70 full-width-carousel-fix">
            <div className="container">
                <div className="row">

                    <div className="col-xl-12">
                        <div className="section-headline margin-top-0 margin-bottom-25">
                            <h3>YouTube</h3>
                        </div>
                    </div>

                    <div className="col-xl-12">
                        <div >
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/2gKOKNDls5I" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                        </div>                            
                        <div>
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/0pGdqVU8jcs" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                        </div>
                    </div>

                </div>
            </div>
        </div>
  );
};
